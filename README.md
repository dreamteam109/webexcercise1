# WebExcercise1

## Running instructions

1. `npm install`
2. `npm start`
3.  * Clients - Open `localhost:8080/?screen=1` for example in your browser (you can change 1 to 2/3)
    * Admin - Open `localhost:8080/admin` (default username: "admin" default password: "admin")

* To reinitialize the database to the default values: stop the server, and start again.
