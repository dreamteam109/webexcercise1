const fs = require("fs");
const fsPromises = fs.promises;
const { ads } = require("./data/ads");

const clearDB = async (db, collections) =>
  await Promise.all(collections.map(collection => db.collection(collection).deleteMany({})))

const insertAds = async (db) => {
  await db.collection("ads").insertMany(ads);
}

const insertTemplates = async (db) => {
  const templatesFolder = "./data/templates/";
  const files = await fsPromises.readdir(templatesFolder);
  const templates = await Promise.all(files.map(async name => {
    const filePath = templatesFolder + name;
    const contents = await fsPromises.readFile(filePath, "utf-8");
    return { name, contents };
  }));

  await db.collection("templates").insertMany(templates);
};

const insertImages = async (db) => {
  const imagesFolder = "./data/images/";
  const files = await fsPromises.readdir(imagesFolder);
  const images = await Promise.all(files.map(async name => {
    const filePath = imagesFolder + name;
    const contents = await fsPromises.readFile(filePath, "binary");
    return { name, contents };
  }));

  await db.collection("images").insertMany(images);
}

const createAdmin = async (db) => {
  await db.collection("admins").insertOne({ username: "admin", password: "admin" });
}

module.exports = async (db) => {
  await clearDB(db, ["ads", "screens", "templates", "images", "admins"]);
  await insertAds(db);
  await insertTemplates(db);
  await insertImages(db);
  await createAdmin(db);
};
