const ads = [
  {
    name: "ad1",
    texts: ["text 1", "text 2", "text 3", "text 4"],
    images: ["./images/image1.jpeg", "./images/image2.jpeg"],
    template: "./templates/templateA.html",
    displayLength: 5,
    schedules: [
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [true, true, true, true, true, true, true],
        startTime: "06:00",
        endTime: "22:00",
      },
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [false, false, false, true, false, false, false],
        startTime: "13:00",
        endTime: "20:00",
      }
    ],
    screenIds: [1, 2]
  },
  {
    name: "ad2",
    texts: [
      "text 1",
      "text 2",
      "text 3",
      "text 4",
      "text 5",
      "text 6",
      "text 7",
      "text 8",
      "text 9",
      "text 10",
    ],
    images: ["./images/image3.jpeg"],
    template: "./templates/templateB.html",
    displayLength: 5,
    schedules: [
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [true, true, true, true, true, true, true],
        startTime: "00:00",
        endTime: "23:59",
      },
    ],
    screenIds: [1, 2, 3]
  },
  {
    name: "ad3",
    texts: [],
    images: [],
    template: "./templates/templateC.html",
    displayLength: 5,
    schedules: [
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [true, true, true, true, true, true, true],
        startTime: "08:00",
        endTime: "22:00",
      },
    ],
    screenIds: [2, 3]
  },
  {
    name: "ad4",
    texts: ["text 1", "text 2"],
    images: [],
    template: "./templates/templateA.html",
    displayLength: 5,
    schedules: [
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [true, true, true, true, true, true, true],
        startTime: "15:00",
        endTime: "19:00",
      },
    ],
    screenIds: [1]
  },
  {
    name: "ad5",
    texts: [
      "text 1",
      "text 2",
      "text 3",
      "text 4",
      "text 5",
      "text 6",
      "text 7",
    ],
    images: ["./images/image4.jpeg", "./images/image5.jpeg"],
    template: "./templates/templateB.html",
    displayLength: 5,
    schedules: [
      {
        startDate: "2021-01-01",
        endDate: "2023-01-01",
        days: [true, true, true, true, true, true, true],
        startTime: "01:00",
        endTime: "23:00",
      },
    ],
    screenIds: [3]
  },
];

exports.ads = ads;
