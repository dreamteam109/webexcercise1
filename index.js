const express = require("express");
const path = require("path");
const initDb = require("./initDb");
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const app = express();
const port = 8080;

(async () => {
  app.use(express.static("public"));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cookieParser());

  const mongodb = require("mongodb");
  const MongoClient = mongodb.MongoClient;
  const connectionURL = "mongodb://localhost:27017";
  const databaseName = "dreamteam";

  let client;

  try {
    client = await MongoClient.connect(connectionURL, {
      useNewUrlParser: true,
    });
  } catch (error) {
    return console.log("Can't connect to db");
  }

  const db = client.db(databaseName);
  await initDb(db);

  app.get("/", async (req, res) => {
    const screen = parseInt(req.query.screen);
    if (isNaN(screen) || screen <= 0) {
      return res.status(400).send("bad screen param");
    }

    await db
      .collection("screens")
      .updateOne(
        { _id: screen },
        { $set: { _id: screen, lastSeen: Date.now() } },
        { upsert: true }
      );

    res.sendFile(path.join(__dirname, "/main.html"));
  });

  app.get("/templates/:templateName", async (req, res) => {
    const { contents: templateContents } = await db.collection("templates").findOne({ name: req.params.templateName });
    res.send(templateContents);
  });

  app.get("/images/:imageName", async (req, res) => {
    const { contents: imageContents } = await db.collection("images").findOne({ name: req.params.imageName });
    res.contentType('image/jpeg');
    res.end(imageContents, "binary");
  });

  const adminLoggedInCheckMiddleware = (req, res, next) => {
    if (req.cookies.isLoggedIn == "true") {
      next();
    } else {
      res.sendStatus(401);
    }
  }

  app.get("/adminLogin", (req, res) => {
    if (req.cookies.isLoggedIn == "true") {
      res.redirect("/admin");
    } else {
      res.sendFile(path.join(__dirname, "/adminLogin.html"));
    }
  })

  app.get("/admin", (req, res) => {
    if (req.cookies.isLoggedIn == "true") {
      res.sendFile(path.join(__dirname, "/admin.html"));
    } else {
      res.redirect("/adminLogin");
    }
  })

  app.post("/login", async (req, res) => {
    const { username, password } = req.body;

    const admin = await db.collection("admins").findOne({ username, password });

    if (admin) {
      res.cookie("isLoggedIn", true);
      res.send();
    }
    else {
      res.sendStatus(401);
    }
  });

  app.post("/setCredentials", adminLoggedInCheckMiddleware, async (req, res) => {
    const { username, password } = req.body;

    if (username === "" || password === "") {
      return res.sendStatus(400);
    }
    await db.collection("admins").updateOne({}, { $set: { username, password } });
    res.cookie("isLoggedIn", false);
    res.send();
  })

  app.get("/connected", async (req, res) => {
    const screens = await db.collection("screens").find({}).toArray();

    const now = Date.now();
    const connected = screens.map(({ _id, lastSeen }) => ({
      screen: _id,
      lastSeen,
      connected: now - lastSeen < 10000,
    }));

    res.send({ connected });
  });

  app.get("/ads", async (req, res) => {
    const screen = parseInt(req.query.screen);
    if (isNaN(screen)) {
      return res.status(400).send("bad screen param");
    }

    const ads = await db.collection("ads").find(screen == -1 ? {} : { screenIds: { $elemMatch: { $eq: screen } } }).toArray();

    res.send({ ads });
  });

  app.delete("/ads/:id", adminLoggedInCheckMiddleware, async (req, res) => {
    await db.collection("ads").deleteOne({ _id: mongodb.ObjectId(req.params.id) });
    res.send();
  })

  app.put("/ads/:id", adminLoggedInCheckMiddleware, async (req, res) => {
    const {_id, ...newAdObject} = req.body;
    await db.collection("ads").updateOne({ _id: mongodb.ObjectId(req.params.id) }, { $set: newAdObject });
    res.send();
  });

  app.post("/ads", adminLoggedInCheckMiddleware, async (req, res) => {
    const {_id, ...newAdObject} = req.body;
    const {insertedId} = await db.collection("ads").insertOne(newAdObject);
    const newAd = await db.collection("ads").findOne({_id: mongodb.ObjectId(insertedId)});
    res.send({ newAd });
  });

  app.post("/ping", async (req, res) => {
    const screen = parseInt(req.query.screen);
    if (isNaN(screen)) {
      return res.status(400).send("bad screen param");
    }

    await db
      .collection("screens")
      .updateOne(
        { _id: screen },
        { $set: { _id: screen, lastSeen: Date.now() } },
        { upsert: true }
      );

    res.send();
  });

  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });
})();
